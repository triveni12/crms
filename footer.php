<!DOCTYPE html>
<html>
<head>
  <title>Footer</title>
</head>
<style>
  .home_footer{
    margin-left: 90px;
    margin-right: 80px;
    display: inline-block;
    margin-top: 80px;
    margin-bottom: 80px;
  }
  .side{
    font-size: 35px;
    display: block;
  }
  .main_footer{
    background-color: lightgray;
  }
  </style>
<body>
  <div class="main_footer">
  <div class="home_footer">
<b class="side">CRMS</b>
<p class="campus">We are creating a website <br> through which students will <br> not need to wander for job <br> placement , they can apply for <br> jobs by registering in this website.</p>
</div>

<div class="home_footer"> 
  <div class="quick">
  <b class="side">Quick Links</b>
  <ul>
    <li  class="last_text"><a href="homepage.php">Home</a></li>
    <li  class="last_text"><a href="about_us.php"> About us</a></li>
    <li  class="last_text"><a href="contact_us.php">Help</a></li>
    <li  class="last_text"><a href="login.php">Login</a></li>
    <li  class="last_text"><a href="#">Register</a></li>
  </ul>
</div>
</div>

<div class="home_footer">
  <b class="side">Information</b>
  <div class="information">
  <ul>
    <li> <b> Phone </b> : <a href="#" class="add"> 07632 242443</a></li>
    <li> <b> Email</b> : <a href="#" class="add">infogpcb@gmail.com</a></li>
    <li> <b>Website </b>: <a href="https://www.gpcbalaghat.ac.in/" class="add"> www.gpcbalaghat.ac.in </a></li>
    <li> <b>Address </b>:<b class="add"> Moti Nagar, Ward No 33,<br> Balaghat, Madhya Pradesh 481001 </b></li>
  </ul>
</div>
</div>

</div>
</body>
</html>