<!DOCTYPE html>
<html>

<head>
    <title> student Sign up page</title>
    </head>
    <style>

input[type=text], input[type=password], input [type=date], input [type=radio] {
  width: 100%;
  padding: 15px;
  margin: 10px 0 22px 0;
  display: inline-block;
  border: none;
  background: lightgray;
  }

input[type=text]:focus, input[type=password]:focus {
  background-color: lightgray;
  outline: none;
}


    input[type=submit] {
        background-color: green;
        color: white;
        padding: 8px 20px;
        border: none;
        cursor: pointer;
        margin-right: 60px;
    }
    
    input[type=submit]:hover {
        background-color: blue;
    }
    
    input[type=reset] {
        background-color: green;
        color: white;
        padding: 8px 20px;
        border: none;
        cursor: pointer;
        float: right;
        margin-right: 60px;
    }
    
    input[type=reset]:hover {
        background-color: blue;
    }
    .astrick{
  color: red;
}
.marq{
    color: red;
}

</style>

<body bgcolor="lightgray">
    <?php
include("header.php");
?>




<?php
    require('db.php');
    // If form submitted, insert values into the database.
    if (isset($_REQUEST['email'])){
        $email = stripslashes($_REQUEST['email']); // removes backslashes
        $email = mysqli_real_escape_string($con,$email); //escapes special characters in a string
        $fullname = stripslashes($_REQUEST['fullname']);
        $fullname = mysqli_real_escape_string($con,$fullname);
        $fname = stripslashes($_REQUEST['fname']);
        $fname = mysqli_real_escape_string($con,$fname);
        $enrollment_no = stripslashes($_REQUEST['enrollment_no']);
        $enrollment_no = mysqli_real_escape_string($con,$enrollment_no);
        $aadhar = stripslashes($_REQUEST['aadhar']);
        $aadhar = mysqli_real_escape_string($con,$aadhar);
        $contact_no = stripslashes($_REQUEST['contact_no']);
        $contact_no = mysqli_real_escape_string($con,$contact_no);
        $dob = stripslashes($_REQUEST['dob']);
        $dob = mysqli_real_escape_string($con,$dob);
        $address = stripslashes($_REQUEST['address']);
        $address = mysqli_real_escape_string($con,$address);
        $category = stripslashes($_REQUEST['category']);
        $category = mysqli_real_escape_string($con,$category);
        $clg_name = stripslashes($_REQUEST['clg_name']);
        $clg_name = mysqli_real_escape_string($con,$clg_name);
        $branch = stripslashes($_REQUEST['branch']);
        $branch = mysqli_real_escape_string($con,$branch);
        $gender = stripslashes($_REQUEST['gender']);
        $gender = mysqli_real_escape_string($con,$gender);
        $what_u_want = stripslashes($_REQUEST['what_u_want']);
        $what_u_want = mysqli_real_escape_string($con,$what_u_want);
        $first = stripslashes($_REQUEST['first']);
        $first = mysqli_real_escape_string($con,$first);
        $second = stripslashes($_REQUEST['second']);
        $second = mysqli_real_escape_string($con,$second);
        $third = stripslashes($_REQUEST['third']);
        $third = mysqli_real_escape_string($con,$third);
        $fourth = stripslashes($_REQUEST['fourth']);
        $fourth = mysqli_real_escape_string($con,$fourth);
        $fifth = stripslashes($_REQUEST['fifth']);
        $fifth = mysqli_real_escape_string($con,$fifth);
        $sixth = stripslashes($_REQUEST['sixth']);
        $sixth = mysqli_real_escape_string($con,$sixth);
        $cgpa = stripslashes($_REQUEST['cgpa']);
        $cgpa = mysqli_real_escape_string($con,$cgpa);
        $passout_year = stripslashes($_REQUEST['passout_year']);
        $passout_year = mysqli_real_escape_string($con,$passout_year);
        $resume = stripslashes($_REQUEST['resume']);
        $resume = mysqli_real_escape_string($con,$resume);
        $password = stripslashes($_REQUEST['password']);
        $password = mysqli_real_escape_string($con,$password);

        $trn_date = date("Y-m-d H:i:s");
        $query = "INSERT into student_registration (email, fullname, fname, enrollment_no, aadhar, contact_no, dob, address, category, clg_name, branch, gender, what_u_want, first, second, third, fourth, fifth, sixth, cgpa, passout_year, resume, password, trn_date) VALUES ('$email', '$fullname', '$fname', '$enrollment_no', '$aadhar', '$contact_no', '$dob', '$address', '$category', '$clg_name', '$branch', '$gender', '$what_u_want', '$first', '$second', '$third', '$fourth','$fifth', '$sixth', '$cgpa', '$passout_year', '$resume', '$password','$trn_date')";
        $result = mysqli_query($con,$query);
        if($result){
            echo "<div class='form'><h3>Now you will be verified by admin for which you have to wait for 24 hours.</h3><br/>Click here to <a href='homepage.php'>Home</a></div>";
        }
    }else{
?>


<br><br><br><br>
    <center>
        <div>
            <fieldset style="background-color: white; height: 2300px; width: 600px;">
                <legend></legend>
                <h1 style="color: indigo;" class="hr">REGISTRATION STUDENT</h1><br>
               <marquee class="marq" >After register you will be verified by admin for which you will have to wait up to 24 hours.</marquee><br><br><br><br>
                <form>  
                    <h2>PERSONAL INFORMATION</h2><br>
                    <table>

                        <tr>
                            <td>Email :<span class="astrick">*</span></td>
                            <td><input type="text" name="email" placeholder="Enter your Email"></td>
                        </tr>

                        <tr>
                            <td> Student Name : <span class="astrick">*</span></td>
                            <td><input type="text" name="fullname" placeholder="Enter your full name" required></td>
                        </tr>
                                
                        <tr>
                            <td> Father's Name :<span class="astrick">*</span></td>
                            <td><input type="text" name="fname" placeholder="Enter your Father's name"></td>
                        </tr>
                        <tr>
                            <td> Enrollment Number : <span class="astrick">*</span></td>
                            <td><input type="text" name="enrollment_no" placeholder="  Your Enrollment number" required></td>
                        </tr>
                        <tr>
                            <td>Aadhar Number : <span class="astrick">*</span></td>
                            <td><input type="text" name="aadhar" placeholder=" Your Aadhar Number"></td>
                        </tr>
                     
                        <tr>
                            <td>Contact Number : <span class="astrick">*</span></td>
                            <td><input type="text" name="contact_no" placeholder=" Your Contact Number"></td>
                        </tr>
                        
                        
                        
                      <tr>
                            <td>Date of Birth :  <span class="astrick">*</span></td>
                            <td><input type="date" name="dob" placeholder="Enter your Date of Birth"></td>
                        </tr>
                        
                            <tr>
                                <td>Address : <span class="astrick">*</span></td>
                                <td>
                                    <textarea class="address-stu" name="address" rows="2" cols="20"> </textarea>
                                </td>
                            </tr>
                            <tr><td>Category : <span class="astrick">*</span></td>
                                <td><select name="category">
                                        <option>Category</option>               
                                        <option>OBC</option>
                                        <option>SC/ST</option>
                                        <option>General</option>
                                        <option>Other</option>
        </select></td>
             
                        </tr>
                        <tr>
                            <td> Name of College : <span class="astrick">*</span></td>
                            <td><input type="text" name="clg_name" placeholder="Enter your College Name"></td>
                        </tr>
                        <td>Branch : <span class="astrick">*</span></td>
                        <td><select name="branch">
                                <option>Branch</option>
                                <option>Computer Science & Engineering</option>
                                <option>Civil</option>
                                <option>Mechanical</option>
                                <option>Electrical</option>
                            </select></td>
                        </tr>
                    
                        <tr>
                            <td>Gender : <span class="astrick">*</span></td>
                            <td>Female <input type="radio" name="gender"> Male <input type="radio" name="gender"> Other <input type="radio" name="gender">
                            </td>
                        </tr>
                        </table><br>
                    <table>
                        <tr>
                            <td>What do you want to get through this website? 
                               <br> Please choose any one of these options.. <span class="astrick">*</span> <br><br>
                                     <select  name="what_u_want">
                                            <option>What you want ? </option>
                                            <option>Job</option>
                                            <option>Training(Industrial/Voccational)</option>
                                            
                                        </select></td>
                        </tr>
                    </table><br><br><br><br>
                    <h2>ACADEMIC HISTORY</h2><br>
                    <table>

                        <tr>
                            <td>Your 1st sem SGPA :<span class="astrick">*</span> </td>
                            <td><input type="text" name="first" placeholder="enter your 1st sem SGPA" required></td>
                        </tr>
                        <tr>
                            <td>Your 2nd sem SGPA : <span class="astrick">*</span></td>
                            <td><input type="text" name="second" placeholder="enter your 2nd sem SGPA" required></td>
                        </tr>

                        <tr>
                            <td>Your 3rd sem SGPA  : </td>
                            <td><input type="text" name="third" placeholder="enter your 3rd sem SGPA" required></td>
                        </tr>

                        <tr>
                            <td>Your 4th sem SGPA : </td>
                            <td><input type="text" name="fourth" placeholder="enter your 4th sem SGPA"></td>
                        </tr>

                        <tr>
                            <td>Your 5th sem SGPA : </td>
                            <td><input type="text" name="fifth" placeholder="enter your 5th sem SGPA"></td>
                        </tr>

                        <tr>
                            <td>Your 6th sem SGPA : </td>
                            <td><input type="text" name="sixth" placeholder="enter your 6th sem SGPA"></td>
                        </tr>

                        <tr>
                            <td>CGPA(1st+2nd+3rd+4th+5th+6th sem) : </td>
                            <td><input type="text" name="cgpa" placeholder="enter your CGPA"></td>
                        </tr>
                        <tr>
                            <td>Diploma pass out year : </td>
                            <td><input type="text" name="passout_year" placeholder="Enter your diploma pass out year"></td>
                        </tr>
                        <tr>
                            <td>Resume :</td>
                            <td><input type="file" name="resume" placeholder="Resume" ></td>
                        </tr>

                    </table>
                    <table>
                        <tr>
                            <td>  Password : <span class="astrick">*</span></td>
                            <td><input type="password" name="password" placeholder="enter password" id="password" required></td>
                        </tr>
                        <tr>
                            <td> Confirm Password : <span class="astrick">*</span></td>
                            <td><input type="password" name="confirm_password" placeholder="confirm password" id="confirm_password" required></td>
                        </tr>
                    </table><br><br>
                    <table>
                        <tr>
                            <td> <input type="submit" name="submit" value="submit" onclick="f();"></td>
                            <td> </td>
                            <td><input type="reset" name="reset" value="reset"> </td>
                        </tr>
                    </table>

                </form>

        </div>
        </fieldset>
    </center>
    <script type="text/javascript">
            function f() {
                alert('please wait .... After registration, you will be verified by the admin for which you will need to wait 24 hours .');
            }
        </script>

 <?php } ?>

</body>
</html>